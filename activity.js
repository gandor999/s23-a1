
// Inserting One
db.rooms.insertOne({ 
	"name": "single",
	"accomodates":  2,
	"price": 1000,
	"description": "A simple room with all the basic necessities",
	"rooms_available": 10,
	"isAvailable": false
});



// Inserting Many

db.rooms.insertMany([{ 
	"name": "double",
	"accomodates":  3,
	"price": 2000,
	"description": "A room fit for a small family going on a vacation",
	"rooms_available": 5,
	"isAvailable": false
},
{
	"accomodates":  4,
	"price": 4000,
	"description": "A room with a queen sized bed perfect for a simple getaway",
	"rooms_available": 15,
	"isAvailable": false
}

]);



// Finding a room with the name "double"

db.rooms.find({"name": "double"});



// Update rooms_available for queen sized bed

db.rooms.updateOne(
        {"price": 4000},
    {
        $set: {
            "rooms_available": 0
        }
    }
);



// Delete all rooms with 0 availabilty

db.rooms.deleteMany({
    "rooms_available": 0
});

